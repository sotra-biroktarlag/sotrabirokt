---
layout: post
title:  "Rapport fra Agdermotet"
date:   2017-11-18 16:16:01 -0600
category:  Innlegg
excerpt : "Kjetil Riisnes og Leif-Helge Molvik har deltatt på agder-møtet i Kristiansand og rapporterer tilbake."
---

Kjetil Riisnes og Leif-Helge Molvik har deltatt på agder-møtet i Kristiansand.

 <!-- more -->


## Fredag 17.11.2017
Dagen startet kl 15.00 og var i sin helhet satt av til foredrag og diskusjoner rundt næringsbirøkt i Norge.

Thomas Dahl startet med å fortelle hvordan næringsbirøkterne gjør det i Sverige. Målet for næringsbirøktere i Sverige er 2 timer pr kube og et snitt på 50 kg honning.

Metodene som ble brukt for å oppnå dette var blant annet å ha jevngamle dronninger i hver bigård. Kubene står i samme bigård i 3 år. År 3 blir kubene brukt til å lage nye avleggere.

År 0 = 40 kuber, 800 kg honning , utjevning og passe på forsituasjonen

År 1 = 40 kuber, 2000 kg honning, utjevning, flyttet opp yngel og sette inn byggevoks for å forhindre sverming.

År 2 = 40 kuber, 2000 kg honning, utjevning, flytte opp yngelrammer til laging av avleggere, taper ikke honning på dette da det er overskytende yngel som blir flyttet til avleggerne.

År 3/0 = Bruker alle biene fra de 40 kubene til å lage 100 avleggere sammen med forseglet yngel fra kubene fra år 2. 40 av kubene går tilbake til vanlig produksjon mens de siste 60 kan selges, eller brukes til utvidelse.

Med denne metoden holder han hele tiden samme kubetall i tillegg til å ha avleggere for salg og har enkel drift i alle bigårdene.

Hadde egen slyngelinje og leverte på 1200 liters tanker.

Innovasjon Norge brukte en time til å fortelle litt om hvilke støtteordninger som finnes for birøktere og hvordan man burde skrive søknaden og legge frem dokumentasjon for å kunne få støtte.

Det er 3 punkter man må kunne gi et godt svar på for at søknaden skal kunne bli vurdert.
1. Er problemet verdt å løse
2. Kan jeg lage en løsning på problemet
3. Kan jeg tjene penger på løsningen

Målet men prosjektet må være å tjene penger og skape arbeidsplasser. Innovasjon Norge støtter lettere varig investeringer i utstyr eks slyngerom eller utvikling av nytt kubemateriell.

Det ble en del spørsmål fra salen angående om innovasjon Norge kunne støtte innkjøp av bifolk eller utgifter til vandreutstyr. Innovasjon Norge har støttet slike tiltak før, men det er avhengig av lokale prioriteringer.

Marko Neven fortalte hvordan Norges birøkterlag arbeider sammen med bondelagene for å påvirke jordbruksoppgjøret og budsjetteringen innen landbruket. Det er viktig at birøkterlagene fremmer Næringsbirøkternes sak lokalt der de bor slik at fylkeslagene i bondelaget og småbrukerlaget kan få dette med i jordbruksforhandlingene. Den siste stortingsmeldingen inneholder flere positive punkter for birøkt.

## Lørdag 18.11.2017
Dagen startet med et historisk tilbakeblikk på Honningcentralens historie.

Kristin Slyngstad tok oss med på en reise fra Honningcentralens begynnelse på 1920 tallet og frem til i dag.
Etter et historisk tilbakeblikk ble det fokusert på nåsituasjonen og utfordringene som ligger forran oss de neste 90 årene.

Honningcentralen har som mål å satse på næringsbirøkterne og få Norsk honningproduksjon opp på tidligere nivåer på over 1500 tonn honning. De ønsker å satse på sortshonning, flytende  onning og tavlehonning for å øke konsumet i Norge. I Norge i dag spiser vi 0,5 kg honning pr innbygger mens i resten av europa er dette tallet 2 kg.

Resten av tiden frem til lunsj var satt av til informasjon rundt sykdomsbekjempelse og en debatt hvor blant annet mattilsynet hadde en representant med i panelet.

Pærebrann er en utfordring for norsk birøkt noen steder i landet.
Det er nå åpnet opp for import av eple og pæretrær nov 2015.
Mattilsynet ønsker å endre flyttedatoen til 15.10. I dag er det 15.09.
Mulig det også blir flere soner å forholde seg til.

Åpen og lukket yngelråte ble mye diskutert og vi fikk også høre hvordan de bekjempet disse sykdommene i Danmark.

Det er viktig å forholde seg til flytterestriksjonene fra mattilsynet når det gjelder kjøp og salg av bier.
Reglene for flytting inn og ut av pærebrannområder kan virke uklare, men grunnregelen er at du skal søke for å flytte ut av pærebrannområde. Når du allerede har fått lov til å flytte ut så kan du flytte videre på utsiden uten å søke på nytt. Skal du da tilbake til et pærebrannområde før sperrefristen utgår så må du søke på nytt.

Etter lunsj fikk vi et innblikk i hvordan en av Danmarks største birøktere behandlet og videreforedlet bivoks. I Danmark er det ikke vanlig å ha noe tavlelager liggende på lager over vinteren. På denne måten får de inn 180 tonn voks årlig fra de ca 100 000 bikubene som er i Danmark. 

Voksen blir smeltet med damp på 150-160 grader men blir ikke autoklavert.

Paul Back Sørensen fra Nordjysk Biavl la også frem tall på sykdomsbildet i Danmark og mente at
hyppig voksskifte var grunnen til at de klarte å holde flere av sykdommene i sjakk.

Neste punkt på programmet var en økt om hygene i bigården.
Nils Martin Nybøle fortalte om sin drift med 400 bifolk og eget dronningavl program.
Han hadde spesielt fokus på kalkyngel og nosema som han selv hadde erfaring med.

### Kalkyngel 
Kan komme v stans i egglegging på våren hvis temperaturen i kuben synker.

Hvordan bli kvitt det?

Bytte dronning, bytte voks, vaske utstyr (kasser brett osv), smelte rammer, avlive svake bifolk.

### Nosema
To varianter: Nosema apis og nosema caranae

Forkorter bienes levetid, dårlig vårutvikling, svake bifolk, dårlig overvintring

Ta ut 60 bier og sent til testing.

Georg Kinch, Storekilt 71, 6720 Fanø
georgkinch@gmail.com

Kan sende prøver fra kuber som skal avles på for å dokumentere at det ikke er nosema sporer
tilstede i kuben.

Steng kubene for å unngå røving. Ta kubene vekk fra bigården så fort det blir oppdaget.
To svake nosemakuber = 1 svak kube.

Hvordan bli kvitt det?

Bytte dronning, bytte voks, vaske utstyr (kasser brett osv), smelte rammer, avlive svake bifolk.


### Lage driftsopplegg med system for vask av utstyr.
Kraftvask fra europris for å vaske kasser.

Bengalack innvendig gjør at det blir lettere å vaske.

Smelt om 15-20 rammer i året pr bifolk.

Pass på at det er nok mat i kuben 3-4 kg, nok mat gir friske bier.


### Når bygger biene voks?
Vårutvikling april til juni, sommertrekket, lyngtrekket, innvintring.

Ås har testet hvordan honningutbyttet henger sammen med bruk av byggevoks.

Like mye honning enten det er ferdig utbygde rammer eller byggevoks.


### Renhold av utstyr.
Rent vann, kubeskrape, vaske hendene, kost, hansker, bidress.

Klokken 16.00 snakket Flemming Vejsnes om viktigheten av gode notater fra bigårdene.
Han presenterte sin egenutviklet kubekort app: www.stadekort.dk

En undersøkelse viser at de som fører kubekort har mindre vintertap.
Kubekortene må brukes for å effektivisere driften, husk å noter ned ting som skal gjøres neste gang.
Ved å avle på gode dronninger kan honningmengden pr kube øke med 100%

Her er noen flere kubekortløsninger som også kan prøves ut:

* www.beetight.com
* www.hivetrack.com
* www.kubekort.no
* www.beerecord.com
* www.myhivelog.org
* www.hivelog.dk

Den siste timen før middag ble brukt til opplæring i mjødproduksjon og ble avsluttet med
prøvesmaking av noen sorter mjød.


## Søndag 19.11.2017
Dagen startet med at Flemming Vejsnes kom med et reisebrev fra Apimondia som i år ble arrangert i Tyrkia. Han oppfordret også alle til å reise på neste Apimondia konferanse som holdes i Montreal i september 2019.

3Neste post på programmet var en innføring fra Tore Valen i hannes metoder for 2 dronningdrift.
Innvintrer 50 kuber i hver bigård, har forkaret på hele året med plastikk på toppen.
Har mål om å produsere 10 tonn honning og levere til honningcentralen.

Dekke eget behov for dronninger minimum 400 per år.
Effektivisere og økonomisere øvrig drift til best kvalitet og lønsomhet.
Bruker plastrammer og ruller på voks.
Lager bunnbrett, toppforere og kubekrakker selv.
Setter inn tett plate i bunn i mars/april samtidig som bunnbrettet sjekkes.

Pollinering ca 90 kuber ute på oppdrag i mai/juni.
Dronningavl starter rundt 17 mai.
Avleggere 10 dager seinere, setter inn dronningceller og lar de klekke i avleggeren.
Høsting, tørking og lagring i juli, forer med kvikkpoll før lyngtrekk.
Bruker beesweeper til høsting av honning.

Etter 15 september er det klart for innvintring og da fores det med 24 kg sukker pr kube.
I november bruker han gamle kuber hvor dronningen skal vekk til å fylle på bier i kubene han skal bruke i produksjon neste år. For at kubene skal bli sterke nok til våren er det viktig at det er helt fult av bier gjennom vinteren.

Roar Ree Kirkevold snakket litt om sin nye bok som blir lansert våren 2018.
Tema for boken er forskjellige driftsmetoder og birøktaktiviteter som brukes i Norge og norden.

Siste post på programmet var en økt om storskala birøkt og avleggerproduksjon fra Poul Back
Sørensen.

Måtte velge seg noen aktiviteter å satse på og valgte da.
* Blomsterhonning
* Lynghonning
* Produsere avleggere for salg
* Pollinering
* Slynger honning for andre birøktere

## I 2017
Mellom 800 og 900 bifolk i produksjonkuber.
Lager 600-700 nye avleggere, solgte 250 avleggere i 2017.
1200 kr + moms for avleggere på 6 rammer
Tapper og selger 1200 kg egen honning.
Besøker 4-5 bigårder pr dag.
Kjøper alle dronningene og noterer inne i kubetaket hvilke dronning det er eks IN 008
2 første bokstaver er initialene til dronningavleren, 008 er nummerserien.
Bytter dronninger etter behov, men har ikke faste rutiner på dette. Slår sammen svake kuber gjennom sesongen.
Henger med plass til 8 europaller, 6 kasser pr palle, bruker trøkk for å løfte pallene inn.

Sjekker kubene 6 ganger i året.
Arbeidskostnad er beregnet til 5 kr pr minutt
For å få dette til så må det god planlegging til og her er de 6 trinnene i prosessen.
1. Sjekke foret og Utvider tidlig med første kasse, men de setter den på bunn for at de skal
beholde varmen oppe i yngelrommet. Biene vil ikke lagre honning under yngelleiet.
2. Andre runde i bigården er å sette på 2-3 skattekasser på kubene. De legger plastikk på
rammene over yngelleiet for å holde på varme og fuktighet. 2 rammer på hver side er åpen
opp til skattekassene. Den øverste skattekassen er bare byggevoks. På denne måten vil biene
fylle første skattkasse helt før de starter å bygge ut den øverste.
3. Tredje besøk bruker de til å plukke vekk sommerhonningen fra skattekasser.
4. Fjerde besøk brukes for å ta vekk resten av honningen. De forer også biene med halvparten
av vinterforet i denne runden.
5. Femte besøk er å gi de siste vinterforet
6. Siste besøk for året er å oxalsyrebehandle og nødforing med 2,5 kg apifonda hvis det er behov.

### Avleggerproduksjon
Lager avleggere på 6 rammer.
Setter inn dronningceller og lar de krype i avleggerne.
Lager 70 avleggere hver uke i 8 uker.
Med denne metoden får de en paringsprosent på 85%-90%
Legger inn 1 eller 2 rammer med bier, en dronningcelle og 2,5 kg apifonda.
Det tar 3-4 uker å få paret en dronning i en kasse med 6 rammer.
Lager doble avleggere første runde.
Mange av avleggerne overvintrer i 6 rammerskube og brukes til å bytte dronning på våren.

Ved kontroll etter 4 uke så setter de ned en eggtavle i avleggere de er usikker på og venter en uke før
de sjekker tavlen for dronningceller. Er det dronningceller der så tar de vekk tavlen og legger inn en
uparet dronning i utspisningsbur.
Ferdigforing i september og oxalsyre i desember.
Avleggerne sertifiseres før salg og selges neste vår etter at dronningen har blitt merket.

Han har 3 måter å skaffe bier til avleggere
Tar bier fra gamle dronninger som ikke skal brukes til honningproduksjon.
Bruker gamle dronninger som skal byttes til å produsere pleiebier til avleggere.
Eller plukke yngeltavler fra de "øde" bigårdene som ikke har trekk resten av sesongen etter
sommerhonningen er høstet.

Leif Helge Molvik & Kjetil Riisnes