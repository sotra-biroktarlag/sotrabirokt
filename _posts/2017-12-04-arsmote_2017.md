---
title: Årsmøte 2017
layout: page
---

Årsmøtet for 2017 i Sotra Birøktarlag ble avholdt på Tranevågen skule den 16. november 2017 klokken 20:00.
21 personer tilstede inkludert gjest og støttemedlem.

 <!-- more -->

## Saker

0. Inkalling ble enstemmig vedtatt
1. Sissel Goodgame ble valgt til ordstyrer og Fredrik Lindseth til referent
1. Agnete Andersen og Kjetil Risnes ble valgt til å underskrive protokollen
1. Årsmeldingen ble enstemmig vedtatt
1. En økning kontigenten ble vedtatt. Satsene for 2018 er 80 for ordinært og familiemedlem og 150kr for støttemedlemskap.
1. Regnskapet for 2017 ble enstemmig vedtatt
1. Budsjettet ble framlagt, men det ble stemt fram en endring på å øke “Kjøp av ustyr-posten” til 40.000,-
1. Kjetil Riisnes er valgt til leder av birøktarlaget for en periode på 1 år
1. Agnete Andersen, Truls Berg og Leif Molvik er valgt som styremedlemmer for en periode på 2 år
1. Trine Nergaard er valgt som første varamedlem og Sissel Goodgame som andre varamedlem, for en periode på 1 år
1. Harold Trellevik valgt til revisor for en periode på 1 år.
1. Sissel Goodgame erstatter John Kåre Stordal. Harold Trellevik blir sittende i valgnemda i en ny periode på 3 år. Frode Lindseth var ikke på gjenvalg og sitter i to år til. Se neste side for en komplett oversikt over styret
1. Det ble diskutert en rekke forslag rundt en arbeidsplan med ideer til aktiviteter for 2018
   - Uformelle samlinger gjennom året for å ha noe sosialt uten noe spesiell agenda. Evt med et lite tema man kan snakke om eller noe ala rammekoking eller rammesnikring.
   - Miniforedrag om å designe etiketter til honning, evt med noen proffesjonelle
   - Frode Lindseth om pollineringsrettet birøkt
   - Miniforedrag om hvordan lage og vedlikeholde et regnskap for birøktere.
   - John Strandabø foreslo et foredrag om økonomi i birøkt. Hvordan få mest ut av birøkten.
1. Fredrik Lindseth og Kjetil Riisnes ble valgt av årsmøtet til utsending for Sotra til Hordaland Birøktarlag sitt årsmøte.
1. Øvrige saker
   - John Strandabø påpekte at laget mangler en tydelig og enkel oversikt over hva utstyr laget har og hvem som har de.

Før årsmøtet presenterte Andreas Kårbø en gjennomgang av driftsåret i bigården sin hvor det var muliget til å stille spørsmål underveis.

Fredrik Lindseth

Møtereferent

4 . desember 2017

| Navn             | Rolle       | Varighet |
| ---------------- | ----------- | -------- |
| Kjetil Riisnes   | Leder       | 1 år     |
| Agnete Andersen  | Styremedlem | 2 år     |
| Truls Berg       | Styremedlem | 2 år     |
| Leif Molvik      | Styremedlem | 2 år     |
| Fredrik Lindseth | Styremedlem | 1 år     |
| Trine Nergaard   | Førstevara  | 1 år     |
| Sissel Goodgame  | Andrevara   | 1 år     |
| Sissel Goodgame  | Valgnemda   | 3 år     |
| Frode Lindseth   | Valgnemda   | 2 år     |
| Harold Trellevik | Valgnemda   | 1 år     |
| Harold Trellevik | Revisor     | 1 år     |

Tabell 1: Fullstendig oversikt over styret og valgnemda etter årsmøtet 2017
