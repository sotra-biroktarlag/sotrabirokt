---
title: Dialogmøte om PolliVest
layout: page
---

PolliVest ønsker å knytte tettere kontakt mot birøkterlagene i Hordaland, og inviterer derfor til dialogmøte for å orientere om prosjektet.

 <!-- more -->

## Møtet finner sted

Fleischers Hotel

Evangervegen 13, 5704 Voss

Onsdag 27.februar

kl 17:00-20:00

**_Interesserte bes melde seg til laget [på epost](mailto:sotra.biroektarlag@gmail.com) innen 22. februar så laget kan videreformidle hvor mange som kommer_**

## Tema for dagen

- PolliVest – Status og viktigste erfaringer
- Samarbeid mellom dyrker og røkter
- Hva kan prosjektet tilby røkterne?
- Hvilke ønsker har røkterne?
- Ymse

> PolliVest er et treårig pilotprosjekt finansiert av Grofondet, Norges Birøkterlag og Honningcentralen, med egeninnsats fra FMLA, NLR og Hardanger Fjordfrukt, Sognefrukt og Innvik Fruktlager.
>
> Hovedmålet i prosjektet er økt fruktavling gjennom å øke bruken av honningbier til pollinering av frukthager. Prosjektet formidler bier til pollineringstjeneste til dyrkere tilknyttet de tre fruktlagrene i prosjektet, og rekruttering av birøktere til pollineringstjeneste er en av de viktigste oppgavene i prosjektet. Vi er nå ved «halvgått løp» og ønsker å knytte tettere kontakt mot lokallag av NB for å om mulig få til et tettere samarbeid i tiden framover.
>
> Målgruppe for dialogmøtet er tillitsvalgte i lokal- og fylkeslag av NB i Hordaland, og medlemmer med interesse for utleie av kuber til pollineringstjeneste.
