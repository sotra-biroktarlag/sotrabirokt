---
layout: post
title: "Mattilsynets Smittesoner"
date: 2018-04-07
category: sykdom
excerpt: |
  En oversikt over smittesonene til Mattilsynet
---

En oversikt over de vedtatte smittesonene som gjelder birøkt kan finnes i [Forskrift om birøkt - vedlegg 1](https://lovdata.no/dokument/SF/forskrift/2009-04-06-416#KAPITTEL_6).

 <!-- more -->

De aktuelle sonene som grenser Hordaland er:

## Region B

### Sone B1

Sogn og Fjordane

### Sone B2

Fedje, Austerheim, Masfjorden, Modalen, Vaksdal, Voss og Ulvik

## Region C

Fylkene Østfold, Akershus, Oslo, Hedmark, Oppland, Sør-Trøndelag, Buskerud, Vestfold, Telemark, Aust-Agder, Vest-Agder og kommunene Lund, Sokndal, Eigersund, Hå, Time, Klepp, Bjerkreim, Gjesdal, Sandnes, Stavanger, Sola, Randaberg og Rennesøy i Rogaland og følgende kommuner i Nord-Trøndelag: Stjørdal, Meråker, Frosta, Leksvik, Levanger, Verdal, Mosvik, Inderøy, Verran, Steinkjer, Namdalseid og Flatanger.

## Region D

Hele Hordaland og følgende kommuner i Rogaland: Bokn, Forsand, Haugesund, Hjelmeland, Karmøy, Kvitsøy, Sauda, Strand, Suldal, Tysvær, Utsira, Vindafjord og Finnøy

### Kart

Her vises sonene i hele landet. Flytt musen til grensene for å se region og sone.

A er blå, B er brun, B1 og B2 er blå, C er rød, D er grønn.

<iframe width="100%" height="300px" frameBorder="0" src="https://umap.openstreetmap.fr/en/map/smittesoner-mattilsynet_210831?scaleControl=false&miniMap=false&scrollWheelZoom=false&zoomControl=true&allowEdit=false&moreControl=true&searchControl=null&tilelayersControl=null&embedControl=null&datalayersControl=true&onLoadPanel=undefined&captionBar=false#8/60.173/6.551"></iframe><p><a href="https://umap.openstreetmap.fr/en/map/smittesoner-mattilsynet_210831">See full screen</a></p>

Norges birøkterlag har også noen kart [her](http://www.norbi.no/kartillustrasjoner.cfm).

Dette innebærer:

- [Forbud](https://lovdata.no/dokument/JB/forskrift/2007-04-25-435/%C2%A75#%C2%A75) mot å flytte/selge bier inn, ut og innen bekjempelsessoner for pærebrann[1] fra 1. mai til 25. september, uten å søke om tillatelse fra Mattilsynet. Se [2] for kommuneliste.
- Bier kan ikke flyttes fra Sogn og Fjordane (sone B1) til sone A eller D, grunnet trakèmidd
- Bier kan ikke flyttes fra fra Oslo/Akershus/østlandet/sørlandet (region C) til Sone A eller D. Dette inkluderer også områder som er sone B2 i region C.
- Det er lov å kjøpe dronninger fra region C og sone B2, dersom dronningprodusent sjekker dronningen for varroasmitte og søker Matttilsynet om tillatelse. I tillegg må følgebier fra sone D og A benyttes (følgebier må sendes til dronningprodusent, disse skal brukes til dronningforsendelsen).
- Man kan kjøpe bier fra Bjørlykke siden han er utenfor B1 og B2 sonene, og i en varroafri sone. Man må bare passe på at biene ikke blir fraktet gjennom en sone med varroa.

- Birøktere i Hordaland (region D) trenger ikke varsle Mattilsynet om varroamidd, jamfør Forskrift om birøkt vedlegg 1
- Birøktere i Sogn og Fjordane (sone B1) trenger ikke varsle Mattilsynet om trakèmidd, jamfør Forskrift om birøkt vedlegg 1

- For de med bier i eks Sogn og Fjordane og vil slynge i Hordaland kan man flytte tavler med honning hvis tavlene har vært biefrie i minst 6 dager.

## Relevante lover

- [https://lovdata.no/dokument/JB/forskrift/2007-04-25-435](Lovdata - Forskrift om tiltak mot pærebrann)
- [https://lovdata.no/dokument/SF/forskrift/2009-04-06-416](Lovdata - Forskrift om birøkt)

## Fotnoter

[1] Bekjempelsessonene for pærebranner, ref Forskrift om tiltak mot pærebrann, paragraf 3 a:

- Haram, Giske og Ålesund i Møre og Romsdal fylke
- Gulen, Flora og Askvoll i Sogn og Fjordane fylke
- Austrheim, Radøy, Lindås, Øygarden, Meland, Askøy, Fjell, Bergen, Sund, Os, Austevoll, Kvinnherad, Fitjar, Stord, Bømlo og Sveio i Hordaland fylke
- Haugesund, Tysvær, Vindafjord, Karmøy, Bokn, Rennesøy, Finnøy, Randaberg, Strand, Hjelmeland, Stavanger, Sola, Sandnes, Klepp, Gjesdal, Time, Hå, Bjerkreim, Eigersund, Lund og Sokndal i Rogaland fylke
- Farsund, Mandal, Søgne og Kristiansand i Vest-Agder fylke

[2] Austrheim, Radøy, Lindås, Øygarden, Meland, Askøy, Fjell, Bergen, Sund, Os, Austevoll, Kvinnherad, Fitjar, Stord, Bømlo og Sveio
