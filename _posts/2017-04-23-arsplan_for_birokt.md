---
layout: post
title: Årsplan for birøkt langs kysten på vestlandet
date: 2017-04-23 16:16:01 -0600
category: birokt
excerpt: |
  Et veiledende dokument for raskt oppslag av hva man må gjøre når for birøkten på kysten av Vestlandet
---

Denne planen er ment som et veiledende dokument for raskt oppslag av hva man må gjøre når for birøkten på kysten av Vestlandet. <!-- more --> Beskrivelser på hvordan man skal gjøre det man må, står ikke her og man må lete andre steder. Sjekk birøktlitteratur (for eksempel Ingar’sis Birøkt, Wikibi, bladet Birøkteren på Norges Birøkterlags nettside, Sotra Birøktarlag sin Facebookside, eller kontakt Sotra Birøktarlag).

Denne planen ble laget for å være en lett tilgjengelig plan man eventuelt kan skrive ut og henge opp og for å være en plan tilpasset birøkt på vestlandet. Den bør også tilpasses den enkeltes birøkt, da tidspunkter og gjøremål kan variere avhengig av hvor du driver birøkten din.

Den siste oppdaterte versjonen av dette dokumentet alltid vil være tilgjengelig her [lenke til Google Drive](https://docs.google.com/document/d/1VKfT4rn64TlX2SRY7So8f8Jj1tOTT_vT9by_r0qFr84/edit?usp=sharing). For å laste ned en PDF fra Google Drive, klikk på “File” i menyen øverst, så “Download as” og velg “PDF Document”.

## Januar/Februar

- Forberedelser mens du venter på ny sesong. Listen nedenfor er ikke i prioritert rekkefølge, og er ikke nødvendigvis fullstendig (den er ment å vise eksempler på forberedelser).
- Sortering av rammer for omsmelting
- Rammesmelting og levering av voks til Honningcentralen
- Skraping av honningrammer i skattekassene etter slynging, klar for ny sesong
- Rengjøring av kasser og maling hvis nødvendig
- Klargjøring av rammer med ny byggevoks til utskifting i yngelkassen på våren
- Opptelling av utstyr og oversikt over hva som må skiftes ut (husk ekstra bunner til å bytte ut med ødelagte/skitne bunnbrett)
- Rengjøring av birøkterutstyr og drakter
- Planlegging av nytt år med birøkt, lage personlig plan basert på Sotra Birøktarlag sin årsplan
  - Hvor mange bikuber skal du ha dette året, skal du utvide og har du nok utstyr
  - Har du for nok for fôr til våren og evt til nødforing? (Hvilken type for skal du benytte – candy, honning fra slynging, sukker, pollenerstatning, etc.?)
  - Hvor mange dronninger trengs å byttes ut (basert på erfaringer med dårlige kuber og siste inspeksjon i høst)
  - Rengjøre inspeksjonsbrett, som skal benyttes til sjekk av vårnedfall for Varroa

## Mars

Bytte og vask av bunnbrettet. Dette skal helst byttes ved lav temperatur, mens helst ikke så lav temperatur at biene sitter i klase. Dette må gjøres før biene tar vårrengjøringen, for å hjelpe biene med denne jobben og dermed spare energi.

Samle avskrapet fra diagnosebrettene/bunnbrettene og brenn det. Det kan være vits å analysere nedfallet i rødsprit for å se etter varroa om man ikke har påvist dette allerede.

Vurder om biene trenger nødfôr som Ambrosia/Apifonda/candy. Biene tar ned etter behov så man kan ikke gi for mye, hvis de ikke trenger gjør det ingenting.
Gi pollenerstatning/kvikkpoll/soyaprotein hvis man vil ha biene klar tidligere i sesongen, til for eksempel blåbærtrekk eller tidlig dronningproduksjon.

Ikke gå ned i kubene dersom det ikke er helt nødvendig nå tidlig i sesongen. Maten bør ha vært tilgjengelig for biene i noen dager før man går ned i kuben dersom man mistenker at noe er gale.

## April

Sesongen starter når seljen blomstrer. Det skjer som regel i midten av april og bjørken pleier å blomstre to uker etterpå.

Sørg for at biene ikke har for mye plass, så de klarer å holde varmen hvis det skulle være kaldt i været. Bieantallet synker nå i sesongen før antallet tar seg opp igjen med årets ferske larver.

Dersom biene står på en plass som har lite tilgang på nektar og pollen om våren, bør det vurderes å fôre med flytende sukkerlake/nødfôr nå i april.

Sjekk at bikubene har normal aktivitet, altså trekker inn pollen og har egg/yngel.
Den første skikkelig gjennomgangen av kuben gjøres på en fin dag når det er 12-15 grader i skyggen. Finn dronningen og se etter om du finner egg, yngel og tilstrekkelig med fôr. Få merket dronningen nå dersom den ikke er merket, da det er lettere å finne henne når det er relativt lite bier i bikuben.

Dersom man skal vandre må dette planlegges nå i april med tanke på pærebrannforskriften (bier skal ikke flyttes ut av pærebrannområder etter 1. Mai uten spesifikk tillatelse fra Mattilsynet).

## Mai

Nå må man balanse plassbehov. Det må være plass til yngel for å unngå svermetrang, men ikke så mye plass at biene sliter med å holde varmen i kuben.
Hvis man har innskrenket yngelrommet skal det ikke på med skattekasser før yngelrommet har ti tavler.

Prioriter å bruke utbygde tavler og ikke rammer med byggevoks når du skifter tavler i yngelrommet. Bruk lyse tavler, spesielt for å hindre sykdom (lyse honningtavler fra forrige års skattekasser er fine å benytte).
Hvis kuben oppfører seg normalt er det ikke nødvendig å gå igjennom. Unormalt oppførsel kan være indikasjon på liten aktivitet eller at de ikke har fått inn nok pollen. Da bør denne kuben sjekkes nærmere.

Når yngelrommet er fullt må første skattekassen på en uke senere. Det skal være mye bier i yngelrommet når skattenkassen går på. Legg på dronninggitter mellom yngelrom og skattekasse.

Nå er tiden inne til å lage avleggere eller produsere nye dronninger for inneværende sesong. Husk å sjekke om det er kjønnsmodne droner før du lager avlegger (dronene må være mer enn 2 uker gamle før de er kjønnsmodne og kan pare seg med en dronning). Meld deg på omlarvingsdag i Sotra Birøktarlag.

Den neste skattekassen skal ikke på før den første skattekassen har en høy bistyrke.

I skattekassene bruker man 2-4 rammer med byggevoks og resten utbygde tavler fra i fjor. Ramme med byggevoks plasseres ytterst og utbygde tavler i midten.
Dersom man bruker skattekasse som ren honningkasse (man har dronningitter så dronningen ikke legger egg der) kan mørke tavler med fordel benyttes (spesielt er dette viktig for lynghonning senere på sommeren så de tåler slynging bedre).
Hvis man lar dronningen gå på 2 kasser under dronninggitteret, bruk lyse tavler i begge kassene.

## Juni

Det er nå høysesong for sverming, så nå må man passe på plassen. Det går veldig mye fôr på denne tiden, så det er viktig å passe på at de har nok dersom det ikke er trekk, for eksempel på grunn av lange perioder med dårlig vær. Det trenger ikke ta mer enn tre dager før et bifolk sulter ihjel. Hvis det blir nødvendig, gi biene nødfôr med honningtavler fra i fjor, eller tørrsukker i en ramme bakerst i kuben. Dersom det er mindre enn 2 kg (omtrent 1,5 tavler) fôr må det nødfôres. Skrellevoks kan også brukes som nødfôr i fôrkaret hvis man tar av plexiglasset.

## Juli

Dersom man skal høste sommerhonning bør dette gjøres i begynnelsen av juli. Det bør stå igjen 1.5 - 2 rammer (omtrent 3 kg med honning) så biene har mat til lyngtrekket. Vurder bistyrken om man skal utvide med en ekstra skattekasse. Dersom det er fullt av honning, men bistyrken er for lav til å utvide, ta heller ut noen honningrammer og sett inn utbygde tavler. Honningtavlene kan oppbevares i skattekassene til et sterkere bifolk til de skal slynges.

Hvis man skal på vandring er det verdt å ta seg en tur å sjekke at alt er i orden på vandreplassen at lyngen er klar.
Sjekk for Varroa, og behandle med maursyre dersom middtrykket er høyt før biene blir flyttet på lyngtrekk.

## August

Hvis man har planlagt, eller et ønske om, å lage avleggere, er det nå fin tid å lage de på dersom du trenger nye dronninger til våren. Husk å gjøre det tidlig nok slik at det er nok droner for paring for de nye dronningene.

Også nå gjelder det å passe på plass og fôrmengde og sjekke at alt foregår som normalt i bigården.

En bør vurdere innskuddsfôrer med tørrsukker i august (husk ikke å fore med sukkervann eller fuktig sukker for ikke å få honningforfalskning).
Hvis det skulle være dårlig vær i begynnelsen av lyngtrekket kan biene sulte før de får trekt inn nok nektar.

## September

Datoen for å flytte bikubene ut av pærebrannområder er tidligst 25.september.

Høst årets (lyng-) honning. Tøm kubene helt for honning, men ikke ta ut rammer med yngel. Dersom det er mye yngel i en honningramme, flytt denne opp over dronninggitteret og vent til yngelen har krøpet før du høster honningen.

I slutten av måneden bør man begynne innvintring av bikubene. Man bør vintre inn sterke bisamfunn på 8-10 rammer og fôre bikubene med sukkerlake i fôrkar. Evt bytting av dronning. Svakere samfunn vintres inn på færre rammer.

Fortsett fôring så lenge biene tar ned fôr. Normalt skal biene ta ned cirka 15 kg sukker per bikube. Det er viktig å merke seg at det er 15 kg sukker, og ikke 15 kg sukkerlake.

## Oktober

Det skal være ferdig fôret rundt den 15.oktober (før det blir frost). Fôringskaret kan stå på lenger utover vinteren, biene kan spise av dette fôret før de starter på vinterreservene. Fjern fôrkaret dersom det blir frost i oktober.

Legg på overpakning, slik at kuben er godt isolert for vinteren.

Sjekk kuben og bistyrken og vurdèr om man må innskrenke plassen. Hvis ønskelig kan man også skifte bunnbrett nå. Det lønner seg å vaske alt utstyret så snart det kommer av kuben så slipper man å tenke på dette utover vinteren og våren.

Hvis man har 6 eller flere bifolk får man refundert sukkeravgiften. Man kan søke refusjon til Norges Birøkterlag via “Min side” eller direkte til leder i Sotra Birøktarlag.

Sett honningen til tørk og start slynging så snart det er klart for dette.

## November

Nå bør man behandle med oksalsyre dersom man har påvist varroamidd.

## Desember

Nå er det pause i birøkten, biene sitter i vinterklase til temperaturen stiger på våren. Bruk tiden på å male og reparere utstyr, og gjøre klar til neste sesong.

### Forfattere

Fredrik Lindseth og Kjetil Riisnes, Sotra Birøktarlag.

Takk til Harold Trellevik og Bent Bøtter for deres bidrag til faglig innhold.
Boken Ingar’sis Birøkt har blitt benyttet som referanse.
