---
title: Grasrotandelen
layout: page
---

Flott resultat også i 2018. Fordi 17 spillere valgte Sotra Birøkarlag til sin grasrotmottaker i Norsk Tipping, fikk vi tildelt hele 8734 kroner.

 <!-- more --> Disse kommer godt med når laget skal planlegge medlems'aktiviteter i 2019. Tusen takk til alle dere 😊

Oversikten viser at ytterligere 6 personer i tillegg har valgt laget vårt som sin grasrotmottager i år, takk for at dere valgte oss 😊

Det er enkelt å tilknytte Sotra Birøktarlag som din valgte mottager, enten hos [norsk tipping](https://www.norsk-tipping.no/grasrotandelen#search=913384253) eller hos kommisjonær.

![logo](/assets/pictures/2019-grasrot.jpg)
