---
title: Medlemskap
layout: page
---

I september 2019 var det registrert 89 medlemskap i laget, hvorav 5 er familiemedlemmer og 21 støttemedlemmer. Medlemmene i laget hadde da (estimert) 280 bikuber.

## Ordinært medlemskap

For å bli medlem i laget må man sende epost til [sotra.biroektarlag@gmail.com](mailto:sotra.biroektarlag@gmail.com). Medlemsskontigenten koster 80 kr.

<!-- ## Familiemedlemskap
Familiemedlemskap er tilgjengelig for ... For å opprette familiemedlemmer må man sende epost til [sotra.biroektarlag@gmail.com](mailto:sotra.biroektarlag@gmail.com). Familiemedlemskap koster 80 kr. -->

## Støttemedlemskap

Støttemedlemskap er tilgjengelig hvis man vil delta på Sotra Birøktarlag sine arrangementer og låne lagets utstyr, men er medlem i et annet lag.
For å bli støttemedlem må man sende epost til [sotra.biroektarlag@gmail.com](mailto:sotra.biroektarlag@gmail.com). Støttemedlemskap koster 150,- kr
