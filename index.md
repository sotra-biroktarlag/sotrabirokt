---
title:  
feature_text: |
  # Sotra Birøktarlag
feature_image: "assets/banner.jpg"
image: "assets/logo.jpg"
excerpt:
---

Dette er Sotra Birøktarlag sine nettsider. Her kan du se våre [kommende arrangementer]({{ site.baseurl }}{% link arrangementer.md %})

 <!-- og bilder fra tidligere arrangementer. -->

## Ofte stilte spørsmål

- Jeg vil bli birøkter, hva gjør jeg?
  - Se [Hvordan bli birøktar]({{ site.baseurl }}{% link nybegynner.md %})
- Jeg har funnet en sverm, hva gjør jeg?
  - Se [sverm]({{ site.baseurl }}{% link sverm.md %})
- Jeg vil kjøpe bikuber eller utstyr, hvem selger?
  - Se [kuber]({{ site.baseurl }}{% link kuber.md %})
- Jeg vil låne utstyr av laget. Hva er tilgjengelg og hvor finnes det?
  - Se [utstyr]({{ site.baseurl }}{% link utstyr.md %})

## Medlemskap

Man kan være ordinært medlem eller støttemedlem i Sotra Birøktarlag. Støttemedlemskap er hvis man vil delta på arrangementer og låne utstyr, men er medlem i et annet lag. Se [medlemskap]({{ site.baseurl }}{% link medlemskap.md %}). Det finnes også familiemedlemskap for familier.

## Kontaktinformasjon

For generell kontakt, sende epost til Sotra birøktarlag sin [epost](mailto:sotra.biroektarlag@gmail.com).

Man kan også ta kontakt med leder i Sotra Birøktarlag Fredrik Lindseth [epost](mailto:hei@fredriklindseth.no), [telefon](tel:+4795853805)

- Bankkontonummer - 3628.51.19647
- Organisasjonsnummer - [913 384 253](https://w2.brreg.no/enhet/sok/detalj.jsp?orgnr=913+384+253)
- Vipps - Sotra Birøktarlag
