---
title: Om Sotra Birøkterlag
layout: page
---

## Om laget

Sotra Birøktarlag ble stiftet i 1978 og har hovedsaklig medlemmer fra Øygarden, Fjell, Sund og Bergen, men også fra veldig mange andre kommuner i Hordaland, og tilogmed noen i andre fylker!

## Medlemsutvikling

![Medlemsutvikling](/assets/medlemsutvikling.png)

## Det sittende styret

| Navn             | Rolle       |
| ---------------- | ----------- |
| Fredrik Lindseth | Leder       |
| Agnete Andersen  | Styremedlem |
| Trine Nergaard   | Styremedlem |
| Leif Molvik      | Styremedlem |
| Linn Søvik | Styremedlem |
| Camilla hus       | Førstevara  |
| Cato Erichsen    | Andrevara   |
| Harold Trellevik | Revisor     |

## Lederhistorikk

UNDER ARBEID:

- Truls Berg 2018-2019
- Kjetil Riisnes 2017 - 2018
- Sissel Goodgame 2015 - 2017
- Harold Trellevik xxxx - xxxx
- Tor-Andre Stormark xxxx - xxxx
  ...

<!--
## Kart over medlemmene

TODO
-->

## Kontaktinformasjon

### Postadresse

Fredrik Lindseth

Lægdesvingen 86A, 5096 Bergen

### Bank og betaling

Bankkontonummer - 3628.51.19647

Organisasjonsnummer - 913 384 253

Vipps - Sotra Birøktarlag
