---
title: Arrangementer
layout: page
---

Laget arrangerer som regel minst et nybegynnerkurs og et sertifiseringskurs i året. Det pleier også å være en del mindre arrangementer og arbeidsverksteder for spesielle tema som varroabehandling, omlarvingsdager og produksjon av spesielle kuber og utstyr.

## Kommende og pågående arrangementer

### 2019

Det vil være en rekke arragementer i 2019:

- Fellestur til Vossamøtet
- Rammesmeltedag hos Harold
- Minikurs i økonomi for birøktere
- Omlarveringsdager

Følg med for videre informasjon

#### Mai

#### Sosialt på Bien bar 8. mai 18:00

Vi tar en ny samling på bien snackbar nå som sesongen er i gang igjen.

Vi bestiller bord fra klokken seks og blir sittende utover kvelden.

Vi håper mange kommer innom og deler erfaringer og forteller gode historier fra sesongen. Vi bestiller noen bord og det er mulig å spise og drikke. Spre det gjerne til andre dere kjenner som driver med bier eller er interessert i birøkt.

Man kan parkere enten rett nedenfor krysset, mot legevakten, eller utenfor selve legevakten. Alternativt bystasjonen og ta bybanen opp.

[Facebookarrangement](https://www.facebook.com/events/2365461453778657/)

#### Fagforedrag på Stend 11.mai kl 11:00

Sotra Birøktarlag har i år gleden av å invitere til fagdag på Stend vgs. (Landbruksskolen på Stend, Fanavegen 249, 5244 Fana), 11 mai kl 11:00 i hovedauditoriet.

Se innlegget [Fagforedrag med Sotra Birøktarlag](http://xn--sotrabirktarlag-dub.no/fagforedrag_2019/) for utfyllende informasjon

## Februar

### Nybegynnerkurs 2019

Et nybegynnerkurs er nettop ferdig og et nytt starter 18. februar. Dette koster 2500 kr, pluss 400 for læreboken.
Det er 9 undervisningkvelder pluss ubegrenset tilgang til så mye praksis i lagsbigården som en vil. Det er et arragert opplegg hver uke.

Nybegynnerkursene blir arrangert av Truls Berg og holdt hos Biebutikken i Arna.

### Sertifiseringskurs 2019

23 februar klokken 11:00 – 3. mars klokken 16:00 på Tranevågen Ungdomsskule (Hamramyra 20, 5363 Ågotnes)

Sertifiseringskurs over to helger for de som ønsker å ta eksamen for å bli sertifiserte birøktere i 2019.

Påmelding: https://goo.gl/forms/a46oAaGER6EJPtyu2

Kurset blir arrangert av Sotra Birøktarlag, og kursholder er Sissel Goodgame. For å ta eksamen, må deltakerene også delta på Fagdagen/Fagseminaret arrangert av Fylkeslaget før eller etter kurset. Følg med på epost og websiden til Norges Birøkterlag for dato etterhvert.

Pris for kurset på Sotra er kr 2.000, som må betales før kursstart. I tillegg vil det komme en faktura direkte fra Norgrs Birøkterlag for Fagseminaret, materiell og eksamensavgift.

[Facebooklenke til arrangementet her](https://www.facebook.com/events/169887693949491/) (samme informasjonen som her)

## Tidligere arrangementer

## 2018

## Oktober

### Vossamøtet 2018

Vossamøtet 2018 arrangeres lørdag 27. oktober av Bergenske birøkterlag for birøktere i regionen og andre interesserte. Det blir en faglig og sosial happening hvor vi får oppsummere sesongen, dele erfaringer og kårer vestlandets beste honning.

#### Program

Oppsummering av året ved Dan Sundhordvik og Alf Helge Søyland

Verroatoleranse med Terje Reinertsen
Terje Reinertsen har jobbet med varroatoleranse hos bier siden 1995 og er kjent langt ut over Norges grenser for dette arbeidet.

Roar Ree Kirkevold presenterer sin nye bok om driftsteknikk
Det sies at det finnes tusen måter å drive birøkt på. I sin nye bok har Roar Ree Kirkevold Roar (for mange kjent som forfatteren av boka Ingarsis birøkt) sett på en håndfull av teknikkene.

Siste nytt fra Honningcentralen med Kristin Slyngstad
Kristin Slyngstad er daglig leder i Honningsentralen og birøkter med 12 kuber. Hun leverte i 2017 over 1 tonn honning til Honningcentralen. Kristin Slyngstad skal snakke om egen birøkt og aktuelle saker fra HC.

Tveiten Gård har ordet

Fra 5 til 50, driftsteknikk og økonomi med Marco Neven
Marco Neven er sannsynligvis den birøkter i Norge som kan mest om bruk av honningbier til pollinering. Han skal fortelle om sitt selskap Sognebia, overgangen fra hobby til næring, driftsteknikk, pollinering som nærings- og samfunnsoppdrag og PolliVest prosjektet.

Kåring av Vestlandets beste honning

Det bli middag om kvelden og mingling i baren.

Trykk på lenke for å se detaljert program og påmelding:
https://www.deltager.no/vossamotet_2018

Pris for dagpakke er 495,- for medlemmer av birøkterlag og 595,- for deltagere som ikke medlem av birøkterlag. Pris inkluderer seminar med kaffe, kaker og frukt, samt tre-retters middag om kvelden.

Ektefeller, samboere og kjærester er hjertelig velkommen på Vossamøte og kan velge mellom dagpakke eller kun middag ved påmelding.

Deltagere som ønsker å overnatte må bestille dette selv. Ved å referere til «vossamøte birøkt» tilbyr Fleischers enkeltrom til kr.1395,- Pris per person dobbeltrom er kr. 1095,-.

Påmeldingsfrist er 19. Oktober.

## September

### 13. En samling på Bien Snackbar

Torsdag 7. juni 2018 klokken 18:00 på Bien snackbar (
Fjøsangerveien 30, 5054 Bergen)

Vi tar en ny samling på bien snackbar nå som de fleste er ferdig med høsting og slynging.

Håper mange kommer innom og deler erfaringer og forteller gode historier fra årets sessong.

Spre det gjerne til andre dere kjenner som driver med bier eller er interessert i slikt.

[Facebooklenke til arrangementet her](https://www.facebook.com/events/552261621861046/) (samme informasjonen som her)

## Juni

### 28. Omlarvingsdag krainerbier

Torsdag, 28 juni 2018 fra 17:00-19:00

Sotra Birøktarlag inviterer til omlarvingsdag torsdag 28.juni. Bent Bøtter stiller som erfaren instruktør så her er det bare å ta med dronningramme med cellekopper og ha klar cellebygger i egen bigård. Dette kan være lærerikt selv om du ikke trenger dronninger selv i år.

Arrangementet blir arrangert hos Biebutikken - Norsk Biutstyr as
Peter Jebsens veg 17, 5265 Ytre Arna.

[Facebooklenke til arrangementet her](https://www.facebook.com/events/2219485141606960/) (samme informasjonen som her)

### 7. En samling på bien snackbar

Torsdag 7. juni 2018 klokken 18:00 på Bien snackbar (
Fjøsangerveien 30, 5054 Bergen)

Vi tar en ny samling på bien snackbar nå som sessongen har komt litt i gang.

Forrige gang var vi 16 stykker som var innom og hadde det riktig så hyggelig i noen timer.

Spre det gjerne til andre dere kjenner som driver med bier eller er interessert i slikt.

Da får vi en egen del av bien bar for oss selv til samlingen nå på torsdag.
Si fra i baren når du kommer så viser de deg veien.
Håper på minst like bra oppmøte som sist. 🐝

[Facebooklenke til arrangementet her](https://www.facebook.com/events/211468392798391/) (samme informasjonen som her)

## April

### 19. En samling på bien snackbar

Torsdag, 19 april 2018 klokken 18:00 på Bien snackbar (
Fjøsangerveien 30, 5054 Bergen)

Vi har snakket en stund om at det mangler møtesteder for birøktere utenom de faglige kurs og møter som holdes i Sotra Birøkterlag.

Styret har nå snakket med Bien Bar og de ble kjempe glad når vi ønsket å bruke de til samlinger der nye og gamle birøktere kan treffes og snakke om bier uten at det nudvendigvis er noe fast program.

Vi starter opp med første kveld allerede torsdag 19.04.2018 og håper på at dette kan bli vårt faste møtepunkt fremmover.

[Facebooklenke til arrangementet her](https://www.facebook.com/events/764843310385320/) (samme informasjonen som her)

### Mars

### Sertifiseringskurs 2018

Sotra Birøktarlag tilbyr kompetansekurs for birøktere, som planlegger bli sertifiserte birøktere i år. Kurset avholdes over to helger, med avsluttende eksamen.

For å få formell sertifiserig må deltakerne (i tillegg til eksamen) ha gjennomgått Fagforedraget, som i år blir arrangert av Sunnhordland Birøkterlag til våren. Egen invitasjon til dette følger fra Hordaland- / Bømlo Birøkterlag. Det er også mulig å melde seg på fagforedraget andre steder i landet (se sted/dato www.norbi.no)

Datoer:

- Lørdag, 17. mars kl. 11.00 - 15.00
- Søndag, 18. mars kl.11.00 - 15.00
- Lørdag, 7. April kl. 11.00 - 16.00 (inkl. 1 time avsluttende eksamen)

Pris: kr 1 000,-
Kursavgift dekker foredragsholder, enkel servering, administrasjon og eksamensvakt.

Påmelding via google forms [lenke til skjema her](https://goo.gl/forms/xsSgWXFGmF1hBgyu2)

[Facebooklenke til arrangementet her](https://www.facebook.com/events/142030159817269/)

## Februar 2018

### Nybegynnerkurs i birøkt

Innledende del. Starter 7 februar. Arrangeres av Truls Berg i Biebutikken i Arna sine lokaler.

Kurset koster kr. 2.200 pluss pensumboken som koster kr. 240.-.

Meld deg på ved å sende en epost til Sotra Birøktarlag på [sotra.biroektarlag@gmail.com](mailto:sotra.biroektarlag@gmail.com)

## 2017

### April 2017

#### 23. - Workshop for 5-dronnings paringskube

Sotra Birøktarlag skal arrangere omlarvingsdager også i år, og i den forbindelse vil vi gjerne invitere til å lære kunsten i å produsere 5-dronnings paringskube for de nye dronningene vi lager.

Workshopen er for medlemmer og støttemedlemmer av Sotra Birøktarlag. Kursdeltakerne må selv betale for materiell, som blir utlevert på workshopen til kostpris.

Workshopen blir holdt på Tranevågen Ungdomsskule, Ågotnes.

Dato: 23. april 2017
Start: kl 11.00

### Mai 2017

#### 2. KSL Birøkt Workshop

KSL for Birøkt tilbyr oss birøktere hjelpemiddel for å kunne arbeide mer målrettet og systematisk for å kunne tilby honning som et kvalitetsprodukt.
Sotra Birøktarlag ønsker å tilby vår medlemmer og støttemedlemmer en workshop, som skal se nærmere på bruken av et kvalitetssystem i birøkten vår.

Dette er ikke et fullstendig KSL kurs, men en introduksjon til KSL og bruk av hjelpemidlene for bedre internkontroll. Vi skal se på hjelpemidler, som fritt kan tas i bruk, selv om vi tar beslutning om ikke formelt å KSL registrere oss.

Workshopen er gratis og tilgjengelig for medlemmer og støttemedlemmer av Sotra Birøktarlag.

Påmelding via [denne lenken](https://goo.gl/forms/ugMe49GvTQ65MXbF2)

#### 3. - 5. Varroatest-"safari" på Sotra

Varroaen har etablert seg på Sotra og vi, i Sotra Birøktarlag, ønsker å tilby våre medlemmer og støttemedlemmer hjelp og testutstyr for å sjekke status i så mange bigårder som mulig.

Testingen foregår med en CO2 tester, som er eid av Sotra Birøktarlag, og birøkterne får selv foreta testen under veiledning.

Grunnen til at vi tester nå i vår, er at vi skal få en oversikt over om vi har Varroa i bigården slik at vi kan planlegge en behandlingsstrategi i tide. Det er viktig å behandle dersom vi har smitte, og det er forskjellige metoder vi kan benytte avhengig av hvilken tid på året vi behandler.

Alle som blir med på Varroatest-"safarien", skal få oppfølging og hjelp til behandling av biene dersom vi finner Varroasmitte.

"Safarien" starter helt sør på Sotra, og vi jobber oss oppover til bigårder oppover øya til vi ender hos våre medlemmer i Øygarden kommune. Vi starter så tidlig på ettermiddagen som mulig, og slutter når det blir for mørkt og kaldt å fortsette.

Det er også mulig å få låne testutstyret fra laget. Meld din interesse på påmeldingsskjemaet.

Klikk på [denne lenken](https://goo.gl/forms/vVWZ1zB7s8AgI19v2) for å melde deg på, eller for å be om å få låne CO2 tester.

Værforbehold.

#### 5.-19 Sertifiseringskurs

#### Omlarvingskurs

En kveld med forberedelser og 2–3 omlarvingsdager. En dag med brune bier og en med krainer.

### August 2017

#### 12. Workshop: En sertifiseringssafari

En praktisk repetisjon av sertifiseringskurset i etterkant. Dette blir holdt i en bigård.

### Februar 2017

#### 23. Nybegynnerkurs
