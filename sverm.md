---
title: Biesverm
layout: page
---

Hvis du oppdager en biesverm er det enklest å ta kontakt med birøktere så kommer de og fjerner den veldig kjapt.

Fjerning av sverm koster i utgangspunktet ingenting, men alt gjøres på frivillig basis så man må stille forventingene der etter. Ta gjerne et bilde så vi vet hva vi går til.

Hvis du enten skriver på [Facebooksiden til laget](https://www.facebook.com/groups/419165308221828/) eller sender en [SMS til Fredrik i laget](tel:+4795853805) så kan han skrive et innlegg på Facebooksiden, og så dukker det opp en birøkter. Det er ikke så viktig om svermen ikke er på Sotra/Øygarden, men hvis du heller ønsker kan du skrive på [Bergenske birøkterlag sin Facebookside](https://www.facebook.com/groups/288499704670746/) så er det et godt alternativ.
