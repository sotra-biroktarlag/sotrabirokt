---
title: Kuber
layout: page
---

Den enkleste måten for å få tak i bikuber kan være snakke med dine medbirøktere. Det er alltid noen som selger avleggere, men det er ofte ikke enkelt å ha oversikt over hvem som har noe tilgjengelig.

Å skrive et innlegg på [Facebooksiden til laget](https://www.facebook.com/groups/419165308221828/) er ofte en grei løsning.

Nye kuber er som regel til salgs i slutten av juni og utover året, men det har vært underskudd av bikuber de siste årene så det kan være litt vanskelig å få tak i.

Merk at det er ikke lov å kjøpe bier mellom de ulike smittesonene til Mattilsynet da de ulike fylkene rundt Hordaland har andre smittsomme sykdommer enn oss og birøkterne og Mattilsynet ønsker ikke å få disse innført.

Her føler en (ufullstendig) liste over selgere av birøktutstyr, bier og dronninger:

#### Utstyr til birøkt

Det er flere butikker i Bergensområdet som selger utstyr

- [Tveiten Gard Bieutstyr](https://www.facebook.com/bieutstyr/) (videreselger for Honning Centralen)
- [Biebutikken i Ytre Arna](https://biebutikken.no/)
- [Øygardsbi i Øygarden](http://www.oygardsbi.com)

Det er også noen selgere som dekker hele landet og sender med posten

- [Finnskog Honning i Finnskogen](http://finnskoghonning.no/)
- [KTD på sørlandet](https://ktd.no/)
- [Apinor v/ Morten Bull](http://www.apinor.no/)

Utenlandske butikker

- [Swienty i Danmark](https://www.swienty.com/?CountryID=34&LanguageId=1&CurrencyId=54)

#### Facebookgrupper for kjøp og salg av bie og bieutstyr

- [Markedsplass for salg av dronninger og avlegger](https://www.facebook.com/groups/209710219784067/)
- [Birøkt kjøp og salg](https://www.facebook.com/groups/723313827750175/)

#### Produsenter av avleggere

- Bjørlykke birøkter utenfor Ålesund har tidligere solgt veldig mange avleggere
- Molvik gard som har mål å selge en del avleggere i 2019

#### Dronningprodusenter

Norges birøkterlag har en liste her [norbi.no/avlsdronningeravlereparestasjoner](http://www.norbi.no/avlsdronningeravlereparestasjoner.cfm])

<!--
- [Røisilien bigård](http://www.bigard.no/no/dronningavl) -->
