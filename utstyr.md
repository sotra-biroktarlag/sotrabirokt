---
title: Utstyr i laget
layout: page
---

Sotra Birøktarlag har noe utstyr tilgjengelig for medlemmene

- Motorisert slynge som tar fire rammer
- Sil til slyngen
- Sjøllis honningløsner (se [her](https://docs.google.com/spreadsheets/d/1dF04wp7FZcN557cxDxFvVmD0hRrq6gYOzB1u55_0xxI/edit?usp=sharing) for oversikt over hvor den befinner seg)
- Stor stasjonær vokssmelter som tar 40 rammer (står hos Harold)
- 6–8 rammers vokssmelter
- CO2 varroatester
- Roll-up plakater til utlån til feks stand
- Mikroskop

Utstyret kan lånes ved å sende epost til [sotra.biroektarlag@gmail.com](mailto:sotra.biroektarlag@gmail.com) eller ved å forhøre seg på [Facebooksiden til laget](https://www.facebook.com/groups/419165308221828/) for å høre hvor utstyret befinner seg.

Utstyret er også tilgjengelig for utlån til støttemedlemmer.
