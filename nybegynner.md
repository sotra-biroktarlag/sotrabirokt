---
title: Hvordan bli birøktar
layout: page
---

Den vanlige måten å bli birøkter på er å melde seg opp til nybegynnerkurs hos Sotra Birøktarlag. Disse kursene begynner vanligvis på slutten av året eller tidlig på våren og går gjennom hele sesongen. Det er som regel ni kvelder med teori og tre økter med praksis i en bigård. Kursene koster omtrent 3000,- kr pluss en lærebok som koster 400,- inkludert medlemskap i birøktarlaget.

Iløpet av kurset hjelper vi alle med å få tak i nødvendig utstyr og bifolk.

Se [listen over arrangementer]({{ site.baseurl }}{% link arrangementer.md %}) for oversikt over pågående kurs.

Er kursene fulle eller allerede startet kan det alikavel lønne seg å melde sin interesse på [denne listen](https://forms.gle/zxB4tCTTgkNEsxaM9) da man kan bli satt på interesseliste, det kan bli startet nye kurs etter behov.

For spørsmål send en epost til [sotra.biroektarlag@gmail.com](mailto:sotra.biroektarlag@gmail.com)